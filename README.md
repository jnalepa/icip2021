# Evolving deep ensembles for detecting COVID-19 in chest X-rays

**Piotr Bosowski, Joanna Bosowska, and Jakub Nalepa**

(Submitted to ICIP 2021)

## Supplementary material

This repository contains the supplementary material to the above-mentioned paper. Specifically, it encompasses:

- **base_models** &mdash; a list of all deep models used in our experimentation (170 models in total), their hyperparameters and classification performance quantified over the unseen test data.
- **performance_profiles** &mdash; the time profiles (for inference) of the selected deep architectures.
- **examples** &mdash; example predictions (TP, FN, FP) together with the description prepared by a human reader.
